import json
import os
import logging
import argparse

aparser = argparse.ArgumentParser(description='Python full-scale client for'\
    +'   NetSoul by lastmikoi')
aparser.add_argument('--debug', dest='lvl', action='store_const',\
    const=logging.DEBUG, default=logging.INFO, help='turns on DEBUG level logging')
aparser.add_argument('--logtostderr', dest='to_file', action='store_const',\
    const=False, default=True, help='display logging messages to stderr instead'\
    +'of writing to file')
args = aparser.parse_args()

FORMAT = \
    "[%(levelname)s]%(asctime)s|%(threadName)s:%(message)s|%(filename)s@Line#%(lineno)d"

if args.to_file:
  logging.basicConfig(filename=os.path.expanduser('~') + "/.lastsoul.log",level=args.lvl,format=FORMAT)
else:
  logging.basicConfig(level=args.lvl,format=FORMAT)
std_answer = r"rep (\d+) -- ([^\Z]+)"
usercmd_handler = r"user_cmd "\
    +r"(\d+):(\w+):(\d+)/(\d+):(\w+)@([^:]+):([^:]+):([^:]+):(\w+) "

def get_config(section_name, item_name):
  try:
    with open(os.path.expanduser('~') + '/.lastsoul.cfg') as configfile:
      config = configfile.read()
      obj = json.loads(config)
      ret = obj[section_name][item_name]
  except Exception:
    return (None)
  return (ret)
