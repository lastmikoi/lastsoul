#! /bin/env python3
# -*- coding: latin9 -*-

import json
import os

def defl_input(prompt, default):
  usr = input(prompt+"(%s)" % default);
  if len(usr):
    return (usr)
  else:
    return (default)

def main():
  print("Welcome to lastSoul's netSoul client configurator.")
  print("        !!! Beware, this tool will delete your old configuration !!!")
  input("        Press enter to proceed.")
  config = dict()
  config['Connection'] = {
      'server': defl_input("NetSoul server IP/Hostname ? ", 'ns-server.epitech.net'),
      'port': defl_input("NetSoul server port ? ", '4242')
      }
  config['Auth'] = {
      'login': defl_input("Login ? ", ""),
      'passwd': defl_input("Password socks ? ", "")
      }
  config['Misc'] = {
      'userdata': defl_input("UserData ? ", "lastSoul v1.2"),
      'location': defl_input("Location ? ", ""),
      'watch': defl_input("Watchlist (comma-separated) ? ", "")
      }
  data = json.dumps(config)
  try:
    with open(os.path.expanduser('~') + '/.lastsoul.cfg', 'wb') as configfile:
      configfile.write(bytes(data, "latin9"))
      print("Configuration saved.")
  except IOError as e:
    print("Error while saving config : %s" % e)

if __name__ == "__main__":
  main()
